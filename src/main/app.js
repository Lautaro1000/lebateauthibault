/* ***********************************************************
 * TIG - Le bateau de Thibault.
 * Copyright (C) 2018 <Binh-Minh.Bui-Xuan@ens-lyon.org>.
 * GPL version>=3 <http://www.gnu.org/licenses/>.
 * $Id: BateauThibault/app/src/main/app.js updated 2020-03-03 buixuan.
 * ***********************************************************/
require("nativescript-orientation");
require("nativescript-pulltorefresh");

var application = require("application");
application.start({ moduleName: "src/views/accueil/accueil" });
