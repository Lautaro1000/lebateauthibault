/* ***********************************************************
 * TIG - Le bateau de Thibault.
 * Copyright (C) 2018 <Binh-Minh.Bui-Xuan@ens-lyon.org>.
 * GPL version>=3 <http://www.gnu.org/licenses/>.
 * $Id: BateauThibault/app/src/views/shop/shop.js updated 2020-03-03 buixuan.
 * ***********************************************************/
var frameModule = require("ui/frame");
frameModule.topmost().ios.navBarVisibility = "never";

var listViewModule = require("tns-core-modules/ui/list-view");
var dialogsModule = require("ui/dialogs");
var observableModule = require("data/observable")
var ObservableArray = require("data/observable-array").ObservableArray;
var page;
var listView;
var category;
var validation;
var totalPrice;
var hint;
var shipPointName,shipPointAddress,shipPointDate,shipPointIndex;
var date;
var month=["Janvier", "F\u00e9vrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Ao\u00fbt", "Septembre", "Octobre", "Novembre", "D\u00e9cembre"];
var day=["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"];
var TIGListViewModel = require("~/src/main/tig-list-view-model");
var TIGShipPointListViewModel = require("~/src/main/tig-ship-point-list-view-model");
var products = new TIGListViewModel([]);
var shipPoints = new TIGShipPointListViewModel([]);
var showList = new ObservableArray([]);
var pageData = new observableModule.fromObject({
    products: products,
    shipPoints: shipPoints,
    showList: showList
});

exports.loaded = function(args) {
    page = args.object;
    listView = page.getViewById("products");
    listView.visibility="collapsed";
    validation = page.getViewById("validation");
    validation.visibility="collapsed";
    totalPrice = page.getViewById("totalPrice");
    hint = page.getViewById("hint");
    hint.text="Choissisez vos produits";
    shipPointName = page.getViewById("shipPointName");
    shipPointAddress = page.getViewById("shipPointAddress");
    shipPointDate = page.getViewById("shipPointDate");
    shipPointIndex = 0;
    category = page.getViewById("category");
    page.bindingContext = pageData;

    products.empty();
    pageData.set("isLoading", true);
    products.load().then(function() {
        pageData.set("isLoading", false);
        listView.animate({
            opacity: 1,
            duration: 1000
        });
    });
    shipPoints.empty();
    pageData.set("isLoading", true);
    shipPoints.load().then(function() {
        pageData.set("isLoading", false);
        listView.animate({
            opacity: 1,
            duration: 1000
        });
    });

    listView.on(listViewModule.ListView.itemTapEvent, function (args) {
        var id = showList.getItem(args.index).id;
        var selectedArticle;
        products.forEach(function(article){
            if (article.id==id) selectedArticle=article;
        });
        var selectedIndex=products.indexOf(selectedArticle);
        if (!showList.getItem(args.index).cart) {
            products.getItem(selectedIndex).selected=!products.getItem(selectedIndex).selected;
            if (products.getItem(selectedIndex).selected) {
                products.getItem(selectedIndex).number=1;
            } else {
                products.getItem(selectedIndex).number=0;
            }
            listView.refresh();
        } else {
            dialogsModule.action({
                message: "Choississez la quantit\u00e9",
                cancelButtonText: "Annuler",
                actions: ["0", "1", "2", "3", "4", "5", "Ajouter 1", "Enlever 1"]
            }).then(function (result) {
                if(result == "0"){
                    products.getItem(selectedIndex).number=0;
                }else if(result == "1"){
                    products.getItem(selectedIndex).number=1;
                }else if(result == "2"){
                    products.getItem(selectedIndex).number=2;
                }else if(result == "3"){
                    products.getItem(selectedIndex).number=3;
                }else if(result == "4"){
                    products.getItem(selectedIndex).number=4;
                }else if(result == "5"){
                    products.getItem(selectedIndex).number=5;
                }else if(result == "Ajouter 1"){
                    products.getItem(selectedIndex).number++;
                }else if(result == "Enlever 1"){
                    if (products.getItem(selectedIndex).number>0) products.getItem(selectedIndex).number--;
                }
                if (products.getItem(selectedIndex).number==0) products.getItem(selectedIndex).selected=false;
                while (showList.length) {
                    showList.pop();
                }
                var total=0;
                products.forEach(function(article){
                    if (article.selected) {
                        article.cart=true;
                        total+=article.number*article.price;
                        showList.push(article);
                    }
                });
                totalPrice.text="Total:"+total+'\u20AC';
                listView.refresh();
            });
        }
    });
};

var gestures = require("ui/gestures");
exports.swipeListenner = function(args) {
  if (args.direction == gestures.SwipeDirection.right) {
    hint.text="Choissisez vos produits";
    category.visibility="visible";
    listView.visibility="collapsed";
    validation.visibility="collapsed";
  };
};
exports.shop = function() {
    hint.text="Choissisez vos produits";
    category.visibility="visible";
    listView.visibility="collapsed";
    validation.visibility="collapsed";
}
exports.homePage = function() {
    options = {
        title: "Revenir \u00e0 la page d'accueil?",
        message: "Revenir \u00e0 la page d'accueil effacera votre panier actuel.",
        okButtonText: "Oui",
        cancelButtonText: "Non"
    };

    dialogsModule.confirm(options).then(function(result) {
        if (result) { frameModule.topmost().navigate("src/views/accueil/accueil"); }
    });
}
exports.validate = function() {
    var total=0;
    products.forEach(function(article){
        total+=article.number*article.price;
    });
    options = {
        title: "Envoyer votre commande?",
        message: "Envoyer votre commande de "+total+" \u20AC \u00e0 Thibault?",
        okButtonText: "Oui",
        cancelButtonText: "Non"
    };

    dialogsModule.confirm(options).then(function(result) {
        if (result) {
            frameModule.topmost().navigate("src/views/accueil/accueil");
        }
    });
}
exports.shipPoint = function() {
    var choix = [];
    shipPoints.forEach(function(point){
        choix.push(point.name);
    });
    dialogsModule.action({
        message: "Choississez le point de relais",
        cancelButtonText: "Annuler",
        actions: choix
    }).then(function (result) {
        for (i=0;i<shipPoints.length;i++){
            if (result == shipPoints.getItem(i).name) shipPointIndex=i;
        }
        shipPointName.text=shipPoints.getItem(shipPointIndex).name;
        shipPointAddress.text=shipPoints.getItem(shipPointIndex).address;
        date = new Date(shipPoints.getItem(shipPointIndex).date);
        shipPointDate.text=day[date.getDay()]+' '+date.getDate()+' '+month[date.getMonth()]+', \u00e0 partir de '+(date.getHours()-1)+'h.';
    });
}
exports.fish = function() {
    hint.text="Choissisez vos produits";
    while (showList.length) {
        showList.pop();
    }
    products.forEach(function(article){
        if (article.category==0) {
            article.cart=false;
            showList.push(article);
        }
    });
    category.visibility="collapsed";
    listView.visibility="visible";
    validation.visibility="collapsed";
}
exports.seaFood = function() {
    hint.text="Choissisez vos produits";
    while (showList.length) {
        showList.pop();
    }
    products.forEach(function(article){
        if (article.category==1) {
            article.cart=false;
            showList.push(article);
        }
    });
    category.visibility="collapsed";
    listView.visibility="visible";
    validation.visibility="collapsed";
}
exports.crab = function() {
    hint.text="Choissisez vos produits";
    while (showList.length) {
        showList.pop();
    }
    products.forEach(function(article){
        if (article.category==2) {
            article.cart=false;
            showList.push(article);
        }
    });
    category.visibility="collapsed";
    listView.visibility="visible";
    validation.visibility="collapsed";
}
exports.discount = function() {
    hint.text="Choissisez vos produits";
    while (showList.length) {
        showList.pop();
    }
    products.forEach(function(article){
        if (article.discount) {
            article.cart=false;
            showList.push(article);
        }
    });
    category.visibility="collapsed";
    listView.visibility="visible";
    validation.visibility="collapsed";
}
exports.goCart = function() {
    hint.text="Modifiez la quantit\u00e9 en tappant sur chaque produit";
    shipPointName.text=shipPoints.getItem(shipPointIndex).name;
    shipPointAddress.text=shipPoints.getItem(shipPointIndex).address;
    date = new Date(shipPoints.getItem(shipPointIndex).date);
    shipPointDate.text=day[date.getDay()]+' '+date.getDate()+' '+month[date.getMonth()]+', \u00e0 partir de '+(date.getHours()-1)+'h.';
    while (showList.length) {
        showList.pop();
    }
    var total=0;
    products.forEach(function(article){
        if (article.selected) {
            article.cart=true;
            total+=article.number*article.price;
            showList.push(article);
        }
    });
    totalPrice.text="Total:"+total+'\u20AC';
    category.visibility="collapsed";
    listView.visibility="visible";
    validation.visibility="visible";
}
