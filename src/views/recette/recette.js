/* ***********************************************************
 * TIG - Le bateau de Thibault.
 * Copyright (C) 2018 <Binh-Minh.Bui-Xuan@ens-lyon.org>.
 * GPL version>=3 <http://www.gnu.org/licenses/>.
 * $Id: BateauThibault/app/src/views/recette/recette.js updated 2020-03-03 buixuan.
 * ***********************************************************/
var frameModule = require("ui/frame");
frameModule.topmost().ios.navBarVisibility = "never";

var page;
exports.loaded = function(args) {
  page=args.objects;
};

var gestures = require("ui/gestures");
exports.swipeListenner = function(args) {
  if (args.direction == gestures.SwipeDirection.right) { frameModule.goBack(); };
};

exports.homardRecette = function() { frameModule.topmost().navigate("src/views/recette/list/homardRecette"); };
exports.saintJacques = function() { frameModule.topmost().navigate("src/views/recette/list/saintJacques"); };
exports.barRecette = function() { frameModule.topmost().navigate("src/views/recette/list/barRecette"); };
exports.joueDeRaie = function() { frameModule.topmost().navigate("src/views/recette/list/joueDeRaie"); };
exports.tourteauLinguine = function() { frameModule.topmost().navigate("src/views/recette/list/tourteauLinguine"); };
exports.barLegumes = function() { frameModule.topmost().navigate("src/views/recette/list/barLegumes"); };
exports.recetteXYZ = function() { frameModule.topmost().navigate("src/views/recette/list/default"); };
