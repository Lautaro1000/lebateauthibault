import { Component, OnInit } from '@angular/core';
import { PromoService } from '../services/promo/promo.service';
import { NavController } from '@ionic/angular';
import { PanierService } from '../services/panier/panier.service';

@Component({
  selector: 'app-promo',
  templateUrl: './promo.page.html',
  styleUrls: ['./promo.page.scss'],
})
export class PromoPage implements OnInit {

  promos: any[] = [];

  constructor(private PromoService: PromoService, private panierService: PanierService, private navCtrl: NavController) { }

  ngOnInit(): void {
    this.promos = this.PromoService.getPromos();
  }

  onProduitClique(produit: any): void {
    this.panierService.ajouterProduitAuPanier(produit);
  }
}
