import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { RecettesService } from '../services/recettes/recettes.service';


@Component({
  selector: 'app-recette',
  templateUrl: './recette.page.html',
  styleUrls: ['./recette.page.scss'],
})
export class RecettePage implements OnInit {

  recettes: any[] = [];

  constructor(private RecettesService: RecettesService, private navCtrl: NavController) { }

  onClickedDetail(recette: any) {
    this.navCtrl.navigateForward(`/recette-detail/${recette.id}`);
  }

  ngOnInit(): void {
    this.recettes = this.RecettesService.getRecettes();
  }
}
