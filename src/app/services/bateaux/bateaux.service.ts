import { Injectable } from '@angular/core';
import { Bateau } from 'src/app/interface/bateau';

@Injectable({
  providedIn: 'root'
})
export class BateauxService {

  constructor() { }

  getBateaux(): Bateau[] {
    return [
      { id: 1, nom: 'Titanic', numero: '0945566554', img: 'titanic.jpg', description: 'Montez à bord du Bateau le plus sophistiqué et sur du monde!' },
      { id: 2, nom: 'Queen Annes Revange', numero: '0745566554', img: 'barbenoire.jpg', description: 'Le Bateau le plus chalereux et convivial du monde!' },
      { id: 3, nom: 'Costa Concordia', numero: '0345566554', img: 'Costa_Concordia.jpg', description: 'Le 2e Bateau le plus sur du monde.' },
      { id: 4, nom: 'Radeau', numero: '0445566554', img: 'radeau.jpg', description: 'Le bateau le plus spacieux au monde!' },
      { id: 5, nom: 'Cradeau', numero: '0698876553', img: 'crado.jpg', description: 'Le bateau le plus propre au monde!' },
      { id: 6, nom: 'Baratie', numero: '0445561235', img: 'baratie.webp', description: 'Le bateau flotant' },
    ];
  }
}
