import { Injectable } from '@angular/core';
import { Promo } from 'src/app/interface/promo';

@Injectable({
  providedIn: 'root'
})
export class PromoService {

  constructor() { }

  getPromos(): Promo[] {
    return [
      { id: 1, nom: 'Moules de pêche', img: 'promo.jpg', prix: 7 },
    ];
  }
}