import { TestBed } from '@angular/core/testing';

import { CrustacesService } from './crustaces.service';

describe('CrustacesService', () => {
  let service: CrustacesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CrustacesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
