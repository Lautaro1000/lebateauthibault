import { Injectable } from '@angular/core';
import { Crustaces } from 'src/app/interface/crustaces';

@Injectable({
  providedIn: 'root'
})
export class CrustacesService {

  constructor() { }

  getCrustaces(): Crustaces[] {
    return [
      { id: 1, nom: 'Araignées', img: 'crustace.png', prix: 7 },
    ];
  }
}
