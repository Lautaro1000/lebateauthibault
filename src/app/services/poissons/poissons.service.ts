import { Injectable } from '@angular/core';
import { Poissons } from 'src/app/interface/poissons';

@Injectable({
  providedIn: 'root'
})
export class PoissonsService {

  constructor() { }

  getPoissons(): Poissons[] {
    return [
      { id: 1, nom: 'Filet Bar de ligne', img: 'poissons.png', prix: 7 },
      { id: 2, nom: 'Filet Bar de ligne', img: 'poissons.png', prix: 10 },
      { id: 3, nom: 'Aile de raie', img: 'poissons.png', prix: 10 },
      { id: 4, nom: 'Lieu jaune de ligne', img: 'poissons.png', prix: 12 },
      { id: 5, nom: 'Filet Julienne', img: 'poissons.png', prix: 19 },
      { id: 6, nom: 'Bar de ligne', img: 'poissons.png', prix: 30 },
    ];
  }
}
