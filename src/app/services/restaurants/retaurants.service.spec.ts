import { TestBed } from '@angular/core/testing';

import { RetaurantsService } from './retaurants.service';

describe('RetaurantsService', () => {
  let service: RetaurantsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RetaurantsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
