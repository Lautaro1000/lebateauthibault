import { Injectable } from '@angular/core';
import { Restaurant } from 'src/app/interface/restaurant';

@Injectable({
  providedIn: 'root'
})
export class RestaurantsService {

  constructor() { }

  getRestaurants(): Restaurant[] {
    return [
      { id: 1, nom: 'Pedra Alta', numero: '0945566554', img: 'pedra.png', description: 'Restaurant Gastronomique Portugais' },
      { id: 2, nom: 'Tratoria', numero: '0745566554', img: 'tratoria.jpg', description: 'Restaurant Gastronomique Italien' },
      { id: 3, nom: 'Reblochon', numero: '0345566554', img: 're.png', description: 'Restaurant Gastronomique Francais' },
      { id: 4, nom: 'La Paella', numero: '0445566554', img: 'paella.jpeg', description: 'Restaurant Espagnol' },
      { id: 5, nom: 'Les Planches', numero: '0698876553', img: 'poisson.png', description: 'Cuisine de la Mer depuis 2017' },
      { id: 6, nom: 'Nusr-Et', numero: '0445561235', img: 'nusret.jpg', description: 'Restaurant Gastronomique' },
    ];
  }
}
