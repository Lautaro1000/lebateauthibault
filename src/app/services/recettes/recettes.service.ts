import { Injectable } from '@angular/core';
import { Recette } from 'src/app/interface/recette';

@Injectable({
  providedIn: 'root'
})
export class RecettesService {

  constructor() { }

  getRecettes(): Recette[] {
    return [
      { id: 1, nom: 'Bouillabaisse ', img: 'bou.jpg', description: 'Un ragoût de poisson traditionnel du sud de la France, souvent préparé avec une variété de poissons tels que la lotte, la dorade et le rouget. Assaisonné avec des herbes méditerranéennes, de l ail et des tomates.' },
      { id: 2, nom: 'Sushi', img: 'sushi.jpg', description: 'n plat japonais populaire composé de riz vinaigré accompagné de tranches de poisson cru (comme le saumon, le thon ou la dorade), d algues nori et d autres ingrédients tels que l avocat et le concombre.' },
      { id: 3, nom: 'Paella', img: 'paella2.jpeg', description: 'Un plat espagnol à base de riz cuit dans un bouillon savoureux, généralement préparé avec des fruits de mer tels que les crevettes, les moules, le calmar et du poisson. Il est également aromatisé avec du safran et d autres épices.' },
      { id: 4, nom: 'Ceviche', img: 'ce.jpg', description: 'Une spécialité d Amérique latine, le ceviche consiste en des morceaux de poisson ou de fruits de mer crus marinés dans du jus de citron vert, des oignons, des piments et de la coriandre. Servi généralement en entrée.' },
      { id: 5, nom: 'Filet de saumon grillé au citron ', img: 'saumon.jpeg', description: 'Un plat simple et sain où le filet de saumon est assaisonné avec du citron, de l huile d olive, de l ail et des herbes, puis grillé pour obtenir une texture délicieuse.' },
      { id: 6, nom: 'Moules marinières', img: 'moules.jpg', description: 'Un plat français classique où les moules sont cuites dans une sauce à base de vin blanc, d oignons, d ail et de persil. Souvent servi avec des frites' },
    ];
  }
}
