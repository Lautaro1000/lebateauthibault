import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PanierService {

  private contenuPanier: any[] = [];

  constructor() { }

  ajouterProduitAuPanier(produit: any): void {
    const existant = this.contenuPanier.find((p) => p.id === produit.id);

    if (existant) {
      existant.quantite++;
    } else {
      this.contenuPanier.push(produit);
    }
  }

  modifierQuantite(produit: any, quantite: number) {
    const existant = this.contenuPanier.find((p) => p.id === produit.id);

    if (existant) {
      existant.quantite = quantite;
    }
  }

  getContenuPanier(): any[] {
    return this.contenuPanier;
  }

  calculerPrixTotal(): number {
    const prixTotal = this.contenuPanier.reduce((total, produit) => total + produit.prix, 0);
    console.log('Prix total calculé :', prixTotal);
    return prixTotal;
  }
}