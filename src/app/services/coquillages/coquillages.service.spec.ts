import { TestBed } from '@angular/core/testing';

import { CoquillagesService } from './coquillages.service';

describe('CoquillagesService', () => {
  let service: CoquillagesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CoquillagesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
