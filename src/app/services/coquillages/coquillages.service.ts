import { Injectable } from '@angular/core';
import { Coquillage } from 'src/app/interface/coquillage';

@Injectable({
  providedIn: 'root'
})
export class CoquillagesService {

  constructor() { }

  getCoquillages(): Coquillage[] {
    return [
      { id: 1, nom: 'Moules de pêche', img: 'coquillage.png', prix: 7 },
      { id: 2, nom: 'Bouquets cuits', img: 'coquillage.png', prix: 8 },
      { id: 3, nom: 'Huitres N°2 St Vaast', img: 'coquillage.png', prix: 9 },
      { id: 4, nom: 'Huitres N°2 OR St Vaast', img: 'coquillage.png', prix: 12 },
      { id: 5, nom: 'Huitres N°2 St Vaast', img: 'coquillage.png', prix: 19 },
      { id: 6, nom: 'Huitres N°2 OR St Vaast', img: 'coquillage.png', prix: 24 },
      { id: 7, nom: 'Huitres N°2 St Vaast', img: 'coquillage.png', prix: 38 },
      { id: 8, nom: 'Huitres N°2 OR St Vaast', img: 'coquillage.png', prix: 48 }
    ];
  }
}
