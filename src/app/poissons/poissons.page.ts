import { Component, OnInit } from '@angular/core';
import { PoissonsService } from '../services/poissons/poissons.service';
import { NavController } from '@ionic/angular';
import { PanierService } from '../services/panier/panier.service';

@Component({
  selector: 'app-poissons',
  templateUrl: './poissons.page.html',
  styleUrls: ['./poissons.page.scss'],
})
export class PoissonsPage implements OnInit {

  poissons: any[] = [];

  constructor(
    private poissonsService: PoissonsService,
    private panierService: PanierService,
    private navCtrl: NavController
  ) { }

  ngOnInit(): void {
    this.poissons = this.poissonsService.getPoissons();
  }

  onProduitClique(produit: any): void {
    this.panierService.ajouterProduitAuPanier(produit);
  }
}
