import { Component, OnInit } from '@angular/core';
import { CrustacesService } from '../services/crustaces/crustaces.service';
import { NavController } from '@ionic/angular';
import { PanierService } from '../services/panier/panier.service';

@Component({
  selector: 'app-crustaces',
  templateUrl: './crustaces.page.html',
  styleUrls: ['./crustaces.page.scss'],
})
export class CrustacesPage implements OnInit {

  crustaces: any[] = [];

  constructor(private CrustacesService: CrustacesService, private panierService: PanierService, private navCtrl: NavController) { }

  ngOnInit(): void {
    this.crustaces = this.CrustacesService.getCrustaces();
  }

  onProduitClique(produit: any): void {
    this.panierService.ajouterProduitAuPanier(produit);
  }
}