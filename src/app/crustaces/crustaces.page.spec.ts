import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CrustacesPage } from './crustaces.page';

describe('CrustacesPage', () => {
  let component: CrustacesPage;
  let fixture: ComponentFixture<CrustacesPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(CrustacesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
