import { Component, OnInit } from '@angular/core';
import { CoquillagesService } from '../services/coquillages/coquillages.service';
import { NavController } from '@ionic/angular';
import { PanierService } from '../services/panier/panier.service';


@Component({
  selector: 'app-coquillages',
  templateUrl: './coquillages.page.html',
  styleUrls: ['./coquillages.page.scss'],
})
export class CoquillagesPage implements OnInit {

  coquillages: any[] = [];

  constructor(private CoquillagesService: CoquillagesService, private panierService: PanierService, private navCtrl: NavController) { }

  ngOnInit(): void {
    this.coquillages = this.CoquillagesService.getCoquillages();
  }

  onProduitClique(produit: any): void {
    this.panierService.ajouterProduitAuPanier(produit);
  }
}
