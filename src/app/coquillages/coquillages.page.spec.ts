import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CoquillagesPage } from './coquillages.page';

describe('CoquillagesPage', () => {
  let component: CoquillagesPage;
  let fixture: ComponentFixture<CoquillagesPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(CoquillagesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
