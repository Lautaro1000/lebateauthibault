import { Component, OnInit } from '@angular/core';
import { PanierService } from '../services/panier/panier.service';
import { RestaurantsService } from '../services/restaurants/retaurants.service';

@Component({
  selector: 'app-panier',
  templateUrl: './panier.page.html',
  styleUrls: ['./panier.page.scss'],
})
export class PanierPage implements OnInit {

  isModalQuantiteVisible = false;
  produitSelectionne: any;
  quantiteModifiee = 1;
  contenuPanier: any[] = [];
  prixTotal: number = 0;
  restaurants: any[] = [];
  isModalVisible = false;

  constructor(private panierService: PanierService, private RestaurantsService: RestaurantsService) { }

  ngOnInit(): void {
    this.contenuPanier = this.panierService.getContenuPanier();
    this.calculerPrixTotal();
    this.restaurants = this.RestaurantsService.getRestaurants();
  }

  calculerPrixTotal(): number {
    let prixTotal = 0;

    for (const produit of this.contenuPanier) {
      prixTotal += produit.prix * produit.quantite;
    }

    return prixTotal;
  }

  recalculerPrixTotal() {
    this.prixTotal = this.calculerPrixTotal();
  }

  validerCommande(): void {
    this.recalculerPrixTotal();

    // Afficher le modal
    this.isModalVisible = true;
  }

  fermerModal(): void {
    // Fermer le modal
    this.isModalVisible = false;
  }

  viderPanier() {
    this.isModalVisible = false;
    this.contenuPanier = [];
    window.location.reload();
  }

  modifierQuantite(produit: any) {
    this.produitSelectionne = produit;
    this.quantiteModifiee = produit.quantite;
    this.isModalQuantiteVisible = true;

    this.recalculerPrixTotal();
  }

  validerQuantite() {
    if (this.produitSelectionne && this.quantiteModifiee > 0) {
      this.panierService.modifierQuantite(this.produitSelectionne, this.quantiteModifiee);
    }
    this.isModalQuantiteVisible = false;
  }

  fermerModalQuantite() {
    this.isModalQuantiteVisible = false;
  }
}