import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'produits',
    loadChildren: () => import('./produits/produits.module').then(m => m.ProduitsPageModule)
  },
  {
    path: 'restaurant',
    loadChildren: () => import('./restaurant/restaurant.module').then(m => m.RestaurantPageModule)
  },
  {
    path: 'bateaux',
    loadChildren: () => import('./bateaux/bateaux.module').then(m => m.BateauxPageModule)
  },
  {
    path: 'bateau-detail/:id',
    loadChildren: () => import('./bateau-detail/bateau-detail.module').then(m => m.BateauDetailPageModule)
  },
  {
    path: 'restaurant-detail/:id',
    loadChildren: () => import('./restaurant-detail/restaurant-detail.module').then(m => m.RestaurantDetailPageModule)
  },
  {
    path: 'recette',
    loadChildren: () => import('./recette/recette.module').then(m => m.RecettePageModule)
  },
  {
    path: 'recette-detail/:id',
    loadChildren: () => import('./recette-detail/recette-detail.module').then(m => m.RecetteDetailPageModule)
  },
  {
    path: 'poissons',
    loadChildren: () => import('./poissons/poissons.module').then( m => m.PoissonsPageModule)
  },
  {
    path: 'crustaces',
    loadChildren: () => import('./crustaces/crustaces.module').then( m => m.CrustacesPageModule)
  },
  {
    path: 'coquillages',
    loadChildren: () => import('./coquillages/coquillages.module').then( m => m.CoquillagesPageModule)
  },
  {
    path: 'promo',
    loadChildren: () => import('./promo/promo.module').then( m => m.PromoPageModule)
  },
  {
    path: 'panier',
    loadChildren: () => import('./panier/panier.module').then( m => m.PanierPageModule)
  },
  {
    path: 'contact',
    loadChildren: () => import('./contact/contact.module').then( m => m.ContactPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
