import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RecettesService } from '../services/recettes/recettes.service';

@Component({
  selector: 'app-recette-detail',
  templateUrl: './recette-detail.page.html',
  styleUrls: ['./recette-detail.page.scss'],
})
export class RecetteDetailPage implements OnInit {

  recetteDetail: any = [];
  recetteId: any;

  constructor(private activeRoute: ActivatedRoute, private RecettesService: RecettesService) { }

  ngOnInit(): void {
    this.recetteId = this.activeRoute.snapshot.paramMap.get('id');
    this.recetteDetail = this.RecettesService.getRecettes().filter((recette: any) => recette.id == this.recetteId);
  }
}
