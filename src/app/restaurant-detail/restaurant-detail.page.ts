import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RestaurantsService } from '../services/restaurants/retaurants.service';

@Component({
  selector: 'app-restaurant-detail',
  templateUrl: './restaurant-detail.page.html',
  styleUrls: ['./restaurant-detail.page.scss'],
})
export class RestaurantDetailPage implements OnInit {

  restaurantDetail: any = [];
  restaurantId: any;

  constructor(private activeRoute: ActivatedRoute, private RestaurantsService: RestaurantsService) { }

  ngOnInit(): void {
    this.restaurantId = this.activeRoute.snapshot.paramMap.get('id');
    this.restaurantDetail = this.RestaurantsService.getRestaurants().filter((restaurant: any) => restaurant.id == this.restaurantId);
  }
}
