export interface Promo {
    id: number,
    nom: string,
    img: string,
    prix: number
}