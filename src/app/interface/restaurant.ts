export interface Restaurant {
    id: number,
    nom: string,
    numero: string,
    img: string,
    description: string,
}