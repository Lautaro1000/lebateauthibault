export interface Bateau {
    id: number,
    nom: string,
    numero: string,
    img: string,
    description: string,
}