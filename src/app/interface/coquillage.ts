export interface Coquillage {
    id: number,
    nom: string,
    img: string,
    prix: number
}