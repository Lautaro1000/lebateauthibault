export interface Poissons {
    id: number,
    nom: string,
    img: string,
    prix: number
}