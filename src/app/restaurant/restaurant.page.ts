import { Component, OnInit } from '@angular/core';
import { RestaurantsService } from '../services/restaurants/retaurants.service';
import { NavController } from '@ionic/angular';


@Component({
  selector: 'app-restaurant',
  templateUrl: './restaurant.page.html',
  styleUrls: ['./restaurant.page.scss'],
})
export class RestaurantPage implements OnInit {

  restaurants: any[] = [];

  constructor(private RestaurantsService: RestaurantsService, private navCtrl: NavController) { }

  onClickedDetail(restaurant: any) {
    this.navCtrl.navigateForward(`/restaurant-detail/${restaurant.id}`);
  }

  ngOnInit(): void {
    this.restaurants = this.RestaurantsService.getRestaurants();
  }
}
