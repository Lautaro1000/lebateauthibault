import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private route: Router) { }

  onClickedProduits() {
    this.route.navigate(['/produits']);
  }

  onClickedBateaux() {
    this.route.navigate(['/bateaux']);
  }

  onClickedRestaurants() {
    this.route.navigate(['/restaurant']);
  }

  onClickedRecettes() {
    this.route.navigate(['/recette']);
  }

  onClickedContact() {
    this.route.navigate(['/contact']);
  }

}
