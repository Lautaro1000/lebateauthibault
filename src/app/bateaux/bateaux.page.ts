import { Component, OnInit } from '@angular/core';
import { BateauxService } from '../services/bateaux/bateaux.service';
import { NavController } from '@ionic/angular';


@Component({
  selector: 'app-bateaux',
  templateUrl: './bateaux.page.html',
  styleUrls: ['./bateaux.page.scss'],
})
export class BateauxPage implements OnInit {

  bateaux: any[] = [];

  constructor(private BateauxService: BateauxService, private navCtrl: NavController) { }

  onClickedDetail(bateau: any) {
    this.navCtrl.navigateForward(`/bateau-detail/${bateau.id}`);
  }

  ngOnInit(): void {
    this.bateaux = this.BateauxService.getBateaux();
  }
}
