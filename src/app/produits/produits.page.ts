import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-produits',
  templateUrl: './produits.page.html',
  styleUrls: ['./produits.page.scss'],
})
export class ProduitsPage implements OnInit {

  constructor(private route: Router) { }

  onClickedPoisson() {
    this.route.navigate(['/poissons']);
  }

  onClickedCrustaces() {
    this.route.navigate(['/crustaces']);
  }

  onClickedCoquillages() {
    this.route.navigate(['/coquillages']);
  }

  onClickedPromos() {
    this.route.navigate(['/promo']);
  }

  ngOnInit() {
  }

}
