import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BateauxService } from '../services/bateaux/bateaux.service';

@Component({
  selector: 'app-bateau-detail',
  templateUrl: './bateau-detail.page.html',
  styleUrls: ['./bateau-detail.page.scss'],
})
export class BateauDetailPage implements OnInit {

  bateauDetail: any = [];
  bateauId: any;

  constructor(private activeRoute: ActivatedRoute, private BateauxService: BateauxService) { }

  ngOnInit(): void {
    this.bateauId = this.activeRoute.snapshot.paramMap.get('id');
    this.bateauDetail = this.BateauxService.getBateaux().filter((bateau: any) => bateau.id == this.bateauId);
  }
}
